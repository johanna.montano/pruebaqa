# Proyecto de Pruebas Automatizadas con Karate

## Descripción

Este proyecto contiene pruebas automatizadas para distintas APIs usando el framework Karate. Los tests cubren varias funcionalidades, como la gestión de usuarios, la gestión de órdenes en una tienda de mascotas y la consulta de información meteorológica.


## Prerequisitos

Asegúrate de tener instaladas las siguientes herramientas en tu sistema:

- [JDK 8 o superior](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven](https://maven.apache.org/download.cgi)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) (opcional, pero recomendado)

