Feature: users

  Background:
    * url 'https://reqres.in/api'

  Scenario: Consultar un usuario creado por ID
    Given path 'users/2'
    When method GET
    Then status 200
    And match response.data.id == 2

  Scenario: Actualizar los datos de un usuario y verificar
    Given path 'users/2'
    And request { name: 'John', job: 'Software Engineer' }
    When method PUT
    Then status 200
    And match response.name == 'John'
    And match response.job == 'Software Engineer'

    Given path 'users/2'
    When method GET
    Then status 200
    And match response.data.name == 'John'
    And match response.data.job == 'Software Engineer'

  Scenario: Eliminar un usuario del sistema
    Given path 'users/2'
    When method DELETE
    Then status 204

  Scenario: Consultar la lista completa de usuarios y verificar que el usuario eliminado no se encuentra en el sistema
    Given path 'users'
    When method GET
    Then status 200
    And match response.data[*].id != 2
