import com.intuit.karate.junit5.Karate;
public class Ejecucion {
    @Karate.Test
    Karate testAll() {
        return Karate.run(
                "src/test/java/Ejercicio1/users.feature",
                "src/test/java/Ejercicio2/petstore-orders.feature",
                "src/test/java/Ejercicio3/weather-info.feature"
        );
    }
}
