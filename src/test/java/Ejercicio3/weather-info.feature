Feature: OpenWeatherMap API

  Scenario: Obtener la información del clima consultando por nombre de ciudad
    Given url 'https://api.openweathermap.org/data/2.5/weather'
    And param q = 'London'
    And param appid = 'YOUR_API_KEY'
    When method get
    Then status 200
    And match response.name == 'London'

  Scenario: Obtener la información del clima consultando por latitud y longitud
    Given url 'https://api.openweathermap.org/data/2.5/weather'
    And param lat = 35
    And param lon = 139
    And param appid = 'YOUR_API_KEY'
    When method get
    Then status 200
    And match response.coord.lat == 35
    And match response.coord.lon == 139

  Scenario: Obtener la información del clima en formato JSON
    Given url 'https://api.openweathermap.org/data/2.5/weather'
    And param q = 'London'
    And param appid = 'YOUR_API_KEY'
    And param mode = 'json'
    When method get
    Then status 200
    And match response.name == 'London'

  Scenario: Obtener la información del clima en formato XML
    Given url 'https://api.openweathermap.org/data/2.5/weather'
    And param q = 'London'
    And param appid = 'YOUR_API_KEY'
    And param mode = 'xml'
    When method get
    Then status 200
    And match response.weather.city.name == 'London'
