Feature: Petstore Order Management

  Scenario: Consultar una orden de compra
    Given url 'https://petstore.swagger.io/v2/store/order/1'
    When method get
    Then status 200
    And match response.id == 1

  Scenario: Buscar la orden de compra creada
    Given url 'https://petstore.swagger.io/v2/store/order'
    And param id = 1
    When method get
    Then status 200
    And match response.id == 1

  Scenario: Verificar el inventario de ventas
    Given url 'https://petstore.swagger.io/v2/store/inventory'
    When method get
    Then status 200
    And match response.pets > 0

  Scenario: Eliminar una orden de compra
    Given url 'https://petstore.swagger.io/v2/store/order/1'
    When method delete
    Then status 200
